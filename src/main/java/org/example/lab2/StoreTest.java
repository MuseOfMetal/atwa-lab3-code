package org.example.lab2;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.Date;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class StoreTest {

    private static final String baseUrl = "https://petstore.swagger.io/v2";
    private Integer orderId;

    @BeforeClass
    public void setup(){
        RestAssured.basePath=baseUrl;
        RestAssured.defaultParser= Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }

    @Test
    public void testGetInventory(){
        Response response = given().get(baseUrl+"/store/inventory");
        response.then().statusCode(HttpStatus.SC_OK);
    }
    @Test
    public void testAddOrder(){
        orderId = 123;
        Map<String, ?> body = Map.of(
                "id", orderId,
                "petId", 321,
                "quantity", 10,
                "shipDate", "2023-05-23T17:52:46.143Z",
                "status", "placed",
                "complete", true
        );

        Response response = given().body(body).post(baseUrl+"/store/order");
        response.then().statusCode(HttpStatus.SC_OK);
    }
    @Test(dependsOnMethods = "testAddOrder")
    public void testGetAddedOrder(){
        Response response = given().get(baseUrl+"/store/order/"+orderId);
        response.then().statusCode(HttpStatus.SC_OK);
    }
    @Test(dependsOnMethods = "testGetAddedOrder")
    public void testDeleteAddedOrder(){
        Response response = given().delete(baseUrl+"/store/order/"+orderId);
        response.then().statusCode(HttpStatus.SC_OK);
    }
}
